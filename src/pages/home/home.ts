import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  slides = [
    {
      title: "Welcome to Apollo!",
      description: "<b>Apollo</b> target to bring awareness to public about Solar Energy.",
      image: "assets/Apollo.png",
    },
    {
      title: "Source of Energy?",
      description: "The <b>Sun</b> is our main source of energy and it falls on to our planet every hour.",
      image: "assets/1.jpg",
    },
    {
      title: "How Important is Sun Energy?",
      description: "Every Hour of the <b>Sun Energy</b> is enough to cater for entire planet's human population for one whole year.",
      image: "assets/2.jpg",
    },
     {
      title: "How Much Does it Cost?",
      description: "Most imporantly, the Solar Energy is always <b>Free</b>.",
      image: "assets/3.jpg",
    },
     {
      title: "How Does It Work?",
      description: "To make use of the Solar Energy,<b>Solar Photovoltaic System</b> is used to collect solar energy and store it into battery storage.",
      image: "assets/4.jpg",
    },
     {
      title: "What Can it Powered Up?",
      description: "With adequate amount of Solar Energy collecte, it is enough to power up normal household electrical appliance or even <b> Electric Vehicle </b>.",
      image: "assets/5.jpg",
    },
     {
      title: "Does it work on Cloudy Day?",
      description: "Solar PV system absorb solar radiation and convert it to AC supply to power up electrical appliance. Since its solar radiation,it will works even on <b>Cloudy</b> day.",
      image: "assets/6.jpg",
    },
    {
      title: "There will be always excess power!",
      description: "Should the user's daily power consumption is <b>low</b> enough, there will be excess power from the Solar PV system collected.",
      image: "assets/7.jpg",
    },
    {
      title: "What to do for the excess power?",
      description: "Excess power from the Solar PV System can be sell back to the local utility company at FiT rate.",
      image: "assets/8.jpg",
    },

  ];

}
