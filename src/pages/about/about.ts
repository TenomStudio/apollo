import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { NgZone } from '@angular/core';
declare var google;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {
  url: string = 'https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=-37.0131215&lon=174.905616&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly';
  weatherKey: string = '97d6f5ccf4673a1f96bb9a09753a3493';
  lat: number = -36.85;
  lng: number = 174.76;
  items: any;
  avgOutput: any;
  area: string;
  temperature: number;
  humidity: number;
  windSpeed: number;
  windDirection: number;
  weatherIcon: string;
  power: Array<any>;
  errorMessage: string;

  constructor(public navCtrl: NavController, private http: Http, private zone: NgZone) {
    this.power = [
      0, 0, 0
    ]
    this.errorMessage = '';
  }

  ionViewDidLoad() {
    this.weatherIcon = 'http://openweathermap.org/img/w/01d.png';
    this.requestLocationData(this.lat, this.lng);
    this.initAutocomplete();
  }
  initAutocomplete() {
    var self = this;
    var map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: this.lat, lng: this.lng },
      zoom: 13,
      mapTypeId: 'roadmap'
    });
    self.requestWeatherData(this.lat, this.lng);
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {

      self.errorMessage = '';
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        self.requestLocationData(place.geometry.location.lat(), place.geometry.location.lng());
        self.requestWeatherData(place.geometry.location.lat(), place.geometry.location.lng());

        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });


  }
  requestLocationData(lat, lng) {
    this.http.get('https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=' + lat + '&lon=' + lng + '&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly')
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        let hourIndex = (new Date().getTime() - new Date('2018-01-01 00:00:00').getTime()) / (1000 * 60 * 60);
        this.zone.run(() => {
          this.area = data.station_info.city;
          this.items = data.outputs.ac_monthly[new Date().getMonth()];
          this.avgOutput = data.outputs.ac[Math.floor(hourIndex) - 1];
          if (this.avgOutput > 0) {
            this.power[0] = (this.avgOutput / 100).toFixed(1);
            this.power[1] = (this.avgOutput / 2000 * 60).toFixed(0);
            this.power[2] = (this.avgOutput / 5 * 1000 / 3000).toFixed(0);
          }
        });
      },
        error => {
          this.zone.run(() => {
            console.log(error);
            this.errorMessage = "Area not available";
            this.temperature = 0;
            this.humidity = 0;
            this.windDirection = 0;
            this.windSpeed = 0;
          })



        }
      );
  }

  requestWeatherData(lat, lng) {
    this.http.get('https://api.openweathermap.org/data/2.5/weather?units=metric&lat=' + lat + '&lon=' + lng + '&APPID=' + this.weatherKey)
      .map(res => res.json())
      .subscribe(data => {
        this.temperature = data.main.temp;
        this.humidity = data.main.humidity;
        this.windDirection = data.wind.deg;
        this.windSpeed = data.wind.speed;
        this.weatherIcon = 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png';
      });
  }
}
