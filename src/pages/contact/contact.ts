import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { NgZone } from '@angular/core';
declare var google;
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {
  url: string = 'https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=3.1548224&lon=101.7084323&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly';
  items: any;
  avgOutput: any;
  avgOutputLast24hours: number;
  appliances: Array<any>;
  area: string;
  constructor(public navCtrl: NavController, private http: Http, private zone: NgZone) {
    this.appliances = [
      {
        "Appliance": "100W light bulb (Incandescent)",
        "Minimum": "100W",
        "Maximum": "100W",
        "Standby": "0W",
        "References": "[1]"
      },
      {
        "Appliance": "25\" colour TV",
        "Minimum": "150W",
        "Maximum": "150W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "3\" belt sander",
        "Minimum": "1000W",
        "Maximum": "1000W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "60W light bulb (Incandescent)",
        "Minimum": "60W",
        "Maximum": "60W",
        "Standby": "0W",
        "References": "[1]"
      },
      {
        "Appliance": "9\" disc sander",
        "Minimum": "1200W",
        "Maximum": "1200W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Ceiling Fan",
        "Minimum": "25W",
        "Maximum": "75W",
        "Standby": "0W",
        "References": ""
      },
      {
        "Appliance": "Clock radio",
        "Minimum": "1W",
        "Maximum": "2W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Clothes Dryer",
        "Minimum": "1000W",
        "Maximum": "4000W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Coffee Maker",
        "Minimum": "800W",
        "Maximum": "1400W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Cordless Drill Charger",
        "Minimum": "70W",
        "Maximum": "150W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Desktop Computer",
        "Minimum": "100W",
        "Maximum": "450W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Dishwasher",
        "Minimum": "1200W",
        "Maximum": "1500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Electric Blanket",
        "Minimum": "200W",
        "Maximum": "200W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Electric Heater Fan",
        "Minimum": "2000W",
        "Maximum": "3000W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Electric Kettle",
        "Minimum": "1200W",
        "Maximum": "3000W",
        "Standby": "0W",
        "References": ""
      },
      {
        "Appliance": "Electric Mower",
        "Minimum": "1500W",
        "Maximum": "1500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Electric Shaver",
        "Minimum": "15W",
        "Maximum": "20W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Food Blender",
        "Minimum": "300W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Fridge / Freezer",
        "Minimum": "150W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Hair Blow dryer",
        "Minimum": "1800W",
        "Maximum": "2500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Home Air Conditioner",
        "Minimum": "1000W",
        "Maximum": "4000W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Home Internet Router",
        "Minimum": "5W",
        "Maximum": "15W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Inkjet Printer",
        "Minimum": "20W",
        "Maximum": "30W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Inverter Air conditioner",
        "Minimum": "1300W",
        "Maximum": "1800W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Iron",
        "Minimum": "1000W",
        "Maximum": "1000W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Laptop Computer",
        "Minimum": "50W",
        "Maximum": "100W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Lawnmower",
        "Minimum": "1000W",
        "Maximum": "1400W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "LED Light Bulb",
        "Minimum": "7W",
        "Maximum": "10W",
        "Standby": "0W",
        "References": "[1][2]"
      },
      {
        "Appliance": "Microwave",
        "Minimum": "600W",
        "Maximum": "1700W",
        "Standby": "3W",
        "References": "[1][2]"
      },
      {
        "Appliance": "Oven",
        "Minimum": "2150W",
        "Maximum": "2150W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Power Shower",
        "Minimum": "7500W",
        "Maximum": "10500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Rice Cooker",
        "Minimum": "200W",
        "Maximum": "250W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Scanner",
        "Minimum": "10W",
        "Maximum": "18W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Smart Phone Charger",
        "Minimum": "4W",
        "Maximum": "7W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Strimmer",
        "Minimum": "300W",
        "Maximum": "500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Submersible Water Pump",
        "Minimum": "400W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Table Fan",
        "Minimum": "10W",
        "Maximum": "25W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Tablet Charger",
        "Minimum": "10W",
        "Maximum": "15W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Tablet Computer",
        "Minimum": "5W",
        "Maximum": "10W",
        "Standby": "N/A",
        "References": "[1]"
      },
      {
        "Appliance": "Toaster",
        "Minimum": "800W",
        "Maximum": "1800W",
        "Standby": "0W",
        "References": "[1]"
      },
      {
        "Appliance": "TV (19\" colour)",
        "Minimum": "40W",
        "Maximum": "100W",
        "Standby": "1W",
        "References": "[1]"
      },
      {
        "Appliance": "Vacuum Cleaner",
        "Minimum": "200W",
        "Maximum": "700W",
        "Standby": "0W",
        "References": ""
      },
      {
        "Appliance": "Washing Machine",
        "Minimum": "500W",
        "Maximum": "500W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Water Feature",
        "Minimum": "35W",
        "Maximum": "35W",
        "Standby": "N/A",
        "References": ""
      },
      {
        "Appliance": "Water Filter and Cooler",
        "Minimum": "70W",
        "Maximum": "100W",
        "Standby": "N/A",
        "References": "[1]"
      }
    ];
  }

  ionViewDidLoad() {
    this.avgOutputLast24hours = 0;
    this.http.get(this.url)
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        let hourIndex = Math.floor((new Date().getTime() - new Date('2017-01-01').getTime()) / (1000 * 60 * 60)) - 1;
        this.area = data.station_info.city;
        this.items = data.outputs.ac_monthly[new Date().getMonth()];
        for (var i=hourIndex; i>=hourIndex-24; i--) {
          this.avgOutputLast24hours += data.outputs.ac[i];
        }
        for (var j=0; j<this.appliances.length; j++) {

          this.appliances[j].power = Math.floor(this.avgOutputLast24hours/parseInt(this.appliances[j].Minimum));
        }
        this.avgOutput = data.outputs.ac[hourIndex];
      });
    //this.initAutocomplete();
    
  }
  initAutocomplete() {
    var self = this;
    var map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 3.1575046, lng: 101.7099834 },
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {

      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        self.requestLocationData(place);

        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });


  }
  requestLocationData(place) {
    this.http.get('https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=' + place.geometry.location.lat() + '&lon=' + place.geometry.location.lng() + '&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly')
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        let hourIndex = (new Date().getTime() - new Date('2017-01-01').getTime()) / (1000 * 60 * 60);

        this.zone.run(() => {
          this.area = data.station_info.city;
          this.items = data.outputs.ac_monthly[new Date().getMonth()];
          this.avgOutput = data.outputs.ac[Math.floor(hourIndex) - 1];
        });
      });
  }
}
