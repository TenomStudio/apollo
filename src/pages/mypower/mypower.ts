import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { NgZone } from '@angular/core';
import { AlertController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-mypower',
  templateUrl: 'mypower.html'
})



export class MyPowerPage {

  url: string = 'https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=-37.0131215&lon=174.905616&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly';
  items: any;
  avgOutput: any;
  avgOutputLast24hours: number;
  appliances: Array<any>;
  area: string;
  constructor(public navCtrl: NavController, private http: Http, private zone: NgZone, public alertCtrl: AlertController) {
    this.appliances = [
      {
        "Appliance": "100W light bulb (Incandescent)",
        "Minimum": "100W",
        "Maximum": "100W",
        "Standby": "0W",
        "References": "[1]",
        "isRunning": false
      },
      {
        "Appliance": "25\" colour TV",
        "Minimum": "150W",
        "Maximum": "150W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "3\" belt sander",
        "Minimum": "1000W",
        "Maximum": "1000W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "60W light bulb (Incandescent)",
        "Minimum": "60W",
        "Maximum": "60W",
        "Standby": "0W",
        "References": "[1]",
        "isRunning": false
      },
      {
        "Appliance": "9\" disc sander",
        "Minimum": "1200W",
        "Maximum": "1200W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Ceiling Fan",
        "Minimum": "25W",
        "Maximum": "75W",
        "Standby": "0W",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Clock radio",
        "Minimum": "1W",
        "Maximum": "2W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Clothes Dryer",
        "Minimum": "1000W",
        "Maximum": "4000W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Coffee Maker",
        "Minimum": "800W",
        "Maximum": "1400W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Cordless Drill Charger",
        "Minimum": "70W",
        "Maximum": "150W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Desktop Computer",
        "Minimum": "100W",
        "Maximum": "450W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Dishwasher",
        "Minimum": "1200W",
        "Maximum": "1500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Electric Blanket",
        "Minimum": "200W",
        "Maximum": "200W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Electric Heater Fan",
        "Minimum": "2000W",
        "Maximum": "3000W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Electric Kettle",
        "Minimum": "1200W",
        "Maximum": "3000W",
        "Standby": "0W",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Electric Mower",
        "Minimum": "1500W",
        "Maximum": "1500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Electric Shaver",
        "Minimum": "15W",
        "Maximum": "20W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Food Blender",
        "Minimum": "300W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Fridge / Freezer",
        "Minimum": "150W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Hair Blow dryer",
        "Minimum": "1800W",
        "Maximum": "2500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Home Air Conditioner",
        "Minimum": "1000W",
        "Maximum": "4000W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Home Internet Router",
        "Minimum": "5W",
        "Maximum": "15W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Inkjet Printer",
        "Minimum": "20W",
        "Maximum": "30W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Inverter Air conditioner",
        "Minimum": "1300W",
        "Maximum": "1800W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Iron",
        "Minimum": "1000W",
        "Maximum": "1000W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Laptop Computer",
        "Minimum": "50W",
        "Maximum": "100W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Lawnmower",
        "Minimum": "1000W",
        "Maximum": "1400W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "LED Light Bulb",
        "Minimum": "7W",
        "Maximum": "10W",
        "Standby": "0W",
        "References": "[1][2]",
        "isRunning": false
      },
      {
        "Appliance": "Microwave",
        "Minimum": "600W",
        "Maximum": "1700W",
        "Standby": "3W",
        "References": "[1][2]",
        "isRunning": false
      },
      {
        "Appliance": "Oven",
        "Minimum": "2150W",
        "Maximum": "2150W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Power Shower",
        "Minimum": "7500W",
        "Maximum": "10500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Rice Cooker",
        "Minimum": "200W",
        "Maximum": "250W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Scanner",
        "Minimum": "10W",
        "Maximum": "18W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Smart Phone Charger",
        "Minimum": "4W",
        "Maximum": "7W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Strimmer",
        "Minimum": "300W",
        "Maximum": "500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Submersible Water Pump",
        "Minimum": "400W",
        "Maximum": "400W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Table Fan",
        "Minimum": "10W",
        "Maximum": "25W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Tablet Charger",
        "Minimum": "10W",
        "Maximum": "15W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Tablet Computer",
        "Minimum": "5W",
        "Maximum": "10W",
        "Standby": "N/A",
        "References": "[1]",
        "isRunning": false
      },
      {
        "Appliance": "Toaster",
        "Minimum": "800W",
        "Maximum": "1800W",
        "Standby": "0W",
        "References": "[1]",
        "isRunning": false
      },
      {
        "Appliance": "TV (19\" colour)",
        "Minimum": "40W",
        "Maximum": "100W",
        "Standby": "1W",
        "References": "[1]",
        "isRunning": false
      },
      {
        "Appliance": "Vacuum Cleaner",
        "Minimum": "200W",
        "Maximum": "700W",
        "Standby": "0W",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Washing Machine",
        "Minimum": "500W",
        "Maximum": "500W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Water Feature",
        "Minimum": "35W",
        "Maximum": "35W",
        "Standby": "N/A",
        "References": "",
        "isRunning": false
      },
      {
        "Appliance": "Water Filter and Cooler",
        "Minimum": "70W",
        "Maximum": "100W",
        "Standby": "N/A",
        "References": "[1]",
        "isRunning": false
      }
    ];
  }

  showUsage(appliance) {
    let alert = this.alertCtrl.create({
      title: appliance.Appliance + '<br/><br/>',
      subTitle: '<div>Power Usage: ' + appliance.Minimum + '</div><br/><div>Usage Time: ' + appliance.power + ' hours</div>',
      buttons: ['OK']
    });
    alert.present();
  }

  ionViewDidLoad() {
    this.avgOutputLast24hours = 0;
    this.http.get(this.url)
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        let hourIndex = Math.floor((new Date().getTime() - new Date('2018-01-01 00:00:00').getTime()) / (1000 * 60 * 60)) - 1;
        this.area = data.station_info.city;
        this.items = data.outputs.ac_monthly[new Date().getMonth()];
        for (var i = hourIndex; i >= hourIndex - 24; i--) {
          this.avgOutputLast24hours += data.outputs.ac[i];
        }
        for (var j = 0; j < this.appliances.length; j++) {
          this.appliances[j].power = Math.floor(this.avgOutputLast24hours / parseInt(this.appliances[j].Minimum));
        }

        this.avgOutput = data.outputs.ac[hourIndex];
      });
    //this.initAutocomplete();

  }

  requestLocationData(place) {
    this.http.get('https://developer.nrel.gov/api/pvwatts/v5.json?api_key=j7yQWnUTdygB8YRYemGWUG4Xbx0l36WRT5Ly1vS6&lat=' + place.geometry.location.lat() + '&lon=' + place.geometry.location.lng() + '&system_capacity=4&azimuth=180&tilt=40&array_type=1&module_type=1&losses=10&dataset=intl&timeframe=hourly')
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        let hourIndex = (new Date().getTime() - new Date('2018-01-01 00:00:00').getTime()) / (1000 * 60 * 60);

        this.zone.run(() => {
          this.area = data.station_info.city;
          this.items = data.outputs.ac_monthly[new Date().getMonth()];
          this.avgOutput = data.outputs.ac[Math.floor(hourIndex) - 1];
        });
      });
  }
}