import { Pipe, PipeTransform } from '@angular/core';

// Tell Angular2 we're creating a Pipe with TypeScript decorators
@Pipe({
    name: 'fieldPipe'
})
export class fieldPipe implements PipeTransform {

    // transform(items: Array<any>, isRunning: boolean): Array<any> {
    //     return items.filter(item => item.isRunning === isRunning);
    transform(array: Array<string>, args: string): Array<string> {
        array.sort((a: any, b: any) => {
            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            } else {
                return 0;
            }
        });
        return array;
    }


}